let {baseSum} = require('./baseSum.js');

let sum = baseSum([1,4,6], function(value) {
    return value;
});

console.log(sum);

sum = baseSum([
    { id: 1, value: 7 },
    { id: 3, value: 3 },
    { id: 2, value: 4 }
], function (value) {
    return value.value;
});

console.log(sum);


sum = baseSum([
    { id: 1, value: 7 },
    { id: 3, value: 3 },
    { id: 2, value: 4 }
], function (value) {
    return value.id;
});

console.log(sum);
