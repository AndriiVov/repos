let {copyObject} = require('./copyObject.js');

let a = {
    day1: 'Monday',
    day2: 'Tuesday'
};

let b = {
    day3: 'Wednesday',
    day4: 'Thursday'
};



c = copyObject(b, ['day3','day4'], a);

console.log(c);