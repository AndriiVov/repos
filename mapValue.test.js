let {mapValue} = require('./mapValue');

const students = {
    'Mike':  {'student': 'Mike', 'mark': 5, 'surname': 'Hack'},

    'Rob' :  {'student': 'Rob', 'mark': 4, 'surname': 'Storm'} 
};


 c = mapValue(students,({ mark }) => mark);
 v = mapValue(students,({ surname }) => surname);

console.log(c);
console.log(v);






